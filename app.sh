#!/bin/bash

#installing jdk 14
sudo apt-get update && sudo apt-get install openjdk-14-jdk -y

# cloning repository 

repo = "https://oauth2:Wx_9rjzKt6PNnrNC3ixb@gitlab.com/HNicad/az-devops1.git"
dir_name=$(basename "https://oauth2:Wx_9rjzKt6PNnrNC3ixb@gitlab.com/HNicad/az-devops1.git" .git)

if [[ -d "$dir_name" ]]; then
    cd $dir_name
    git pull
else
    git clone "$repo" && cd $dir_name
fi

# making mvnw executable
sudo chmod +x mvnw


# generating jar file
sudo ./mvnw clean package

